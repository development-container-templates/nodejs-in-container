FROM ubuntu:20.04

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install nodejs -y
RUN apt-get install npm -y

# Install your node-frameworks
# RUN npm install -g nodemon
# RUN npm install react-bootstrap bootstrap@5.1.3

CMD ["echo", "Done"]
