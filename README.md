# NodeJS in Container

Based on [this article](https://hackernoon.com/a-better-way-to-develop-node-js-with-docker-cd29d3a0093) and own research.

Developing and containerizing a NodeJS-based application is quite simple and straightforward, given that all the local dependencies are installed. 

But what about not having any local dependencies other than Docker? This approach is not well-documented, and seems to be hard to achieve - but it is the most useful one, since it frees from having local dependencies cluttering the local machine, leading to all sorts of issues regarding dependencies and versioning.

This is the idea: All NodeJS and project related dependencies are only present in a container. The initial setup is happening inside a container as well (although this container is only used once). During development, any Node-specific infrastructure (server, etc.) resides in a container.

On the local machine, only the source code is to be present. This code can easily be edited using Atom, Notepad++ or VS Code (although we highly recommend [VS Codium](https://vscodium.com) as a replacement, since it does not include trackers).

---

## Prerequisites
- Docker
- Texteditor
- Make (Linux, Mac)

---

## Usage

### Creation of the Project-Stub

Assumption: The current directory contains the Dockerfile and is supposed to have the created project stub in a subdirectory.

#### Preparation
1. Adopt the _Dockerfile_-file according to your liking (such as adding Node-frameworks, etc.)

#### Startup
2. Build the docker image (created image will be tagged _node-initializr:latest_): 
`docker build . -t node-initializr`

3. Run the image as container named _node-init_ and connect the current folder as volume in /app: 
`docker run -d -v "$(pwd)":/app -it --name node-init node-initializr /bin/bash`

4. Connect into the running container: 
`docker exec -it node-init /bin/bash`

#### Inside the container
5. Switch to the _/app_-directory: 
`cd /app`

6. Create your project stub (example: Creating a React application with Bootstrap in the _frontend-app_-subdirectory): 
`npx create-react-app frontend-app`

7. Switch into the newly created directory: 
`cd frontend-app`

8. Fix the audit issues: 
`npm audit fix`

9. Done. Exit the container: 
`exit`

#### Cleaning up
10. Stop the _node-init_ container (optional): 
`docker stop node-init`

11. Remove the _node-init_ container (optional): 
`docker rm node-init`

12. Remove the _node-initializr_-image (optional):
`docker rmi node-initializr -f`
